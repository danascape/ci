#!/usr/bin/env bash
# Copyright (C) 2020 Saalim Quadri (iamsaalim)
# SPDX-License-Identifier: GPL-3.0-or-later

# Set tg var.
function sendTG() {
    curl -s "https://api.telegram.org/bot$token/sendmessage" --data "text=${*}&chat_id=-1001278854279&parse_mode=HTML" > /dev/null
}

function trackTG() {
    curl -s "https://api.telegram.org/bot$token/sendmessage" --data "text=${*}&chat_id=@stormbreakerci&parse_mode=HTML&disable_web_page_preview=True" > /dev/null
}

function sendlog() {
    curl -F chat_id="${CHAT_ID}" -F document=@"$LOG" "https://api.telegram.org/bot${token}/sendDocument"
}

# Setup arguments
PROJECT_DIR="$HOME"
KERNEL_DIR="$PROJECT_DIR/kernelsource"
CHAT_ID="-1001278854279"

if [ -f $KERNEL_DIR/out/arch/arm64/boot/Image.gz-dtb ]
then

sendTG "Build Completed

Device :- ${DEVICE}
Branch :- ${BRANCH}

Build Completed in $DIFF seconds, Uploading zip"


trackTG "Build Completed

Device :- ${DEVICE}
Branch :- ${BRANCH}

Build Completed in $DIFF seconds"

# Strip the kernel package

git clone -b $DEVICE https://github.com/stormbreaker-project/AnyKernel3 $HOME/AnyKernel3 --depth 1 || { sendTG "Branch not found in Anykernel repo"; exit 0;}
cp $KERNEL_DIR/out/arch/arm64/boot/Image.gz-dtb $HOME/AnyKernel3/
cd $HOME/AnyKernel3 && make normal

# Upload the zip
ZIP=$(echo $HOME/AnyKernel3/*.zip)
curl -F chat_id="${CHAT_ID}" -F document=@"$ZIP" "https://api.telegram.org/bot${token}/sendDocument"
sendTG "Join @Stormbreakerci to track your build"

else

trackTG "Build Failed

Device :- ${DEVICE}
Branch :- ${BRANCH}

Build Failed in $DIFF seconds"

sendTG "Build Failed

Device :- ${DEVICE}
Branch :- ${BRANCH}

Build Failed in $DIFF seconds"

LOG=$(echo $KERNEL_DIR/logs.txt)
sendlog
exit 1

fi
