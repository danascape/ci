#!/usr/bin/env bash
# Copyright (C) 2020 Saalim Quadri (iamsaalim)
# SPDX-License-Identifier: GPL-3.0-or-later

# Login to Git
echo -e "machine github.com\n  login $GITHUB_TOKEN" > ~/.netrc

# Set tg var.
function sendTG() {
    curl -s "https://api.telegram.org/bot$token/sendmessage" --data "text=${*}&chat_id=-1001278854279&parse_mode=HTML" > /dev/null
}

function trackTG() {
    curl -s "https://api.telegram.org/bot$token/sendmessage" --data "text=${*}&chat_id=@stormbreakerci&parse_mode=HTML&disable_web_page_preview=True" > /dev/null
}

# Setup arguments
PROJECT_DIR="$HOME"
ORG="https://github.com/stormbreaker-project"
KERNEL_DIR="$PROJECT_DIR/kernelsource"
TOOLCHAIN="$PROJECT_DIR/toolchain"
DEVICE="$1"
BRANCH="$2"
DEFCONFIG="$3"
CLANG="$4"
CHAT_ID="-1001278854279"

# Create kerneldir
mkdir -p "$PROJECT_DIR/kernelsource"

# Clone up the source
git clone $ORG/$DEVICE -b $BRANCH $KERNEL_DIR --depth 1 || { sendTG "Your device is not officially supported or wrong branch"; exit 0;}

# Find defconfig
echo "Checking if defconfig exist ($DEVICE)"
if [[ "$DEFCONFIG" == "none" ]];
then
	if [ -f $KERNEL_DIR/arch/arm64/configs/$DEVICE-perf_defconfig ]
	then
	    echo "Starting build"
	elif [ -f $KERNEL_DIR/arch/arm64/configs/vendor/$DEVICE-perf_defconfig ]
	then
	    echo "Starting build"
	else
	    sendTG "Defconfig not found"
	    exit 0
	fi
else
	if [ -f $KERNEL_DIR/arch/arm64/configs/$DEFCONFIG-perf_defconfig ]
	then
	    echo "Starting build"
	elif [ -f $KERNEL_DIR/arch/arm64/configs/vendor/$DEFCONFIG-perf_defconfig ]
	then
	    echo "Starting build"
	else
	    sendTG "Defconfig not found"
	    exit 0
	fi
fi

# Clone toolchain

echo "Cloning toolchains"
git clone --depth=1 https://github.com/stormbreaker-project/aarch64-linux-android-4.9 $TOOLCHAIN/gcc > /dev/null 2>&1
git clone --depth=1 https://github.com/stormbreaker-project/arm-linux-androideabi-4.9 $TOOLCHAIN/gcc_32 > /dev/null 2>&1
git clone --depth 1 https://github.com/sreekfreak995/Clang-11.0.3.git $TOOLCHAIN/clang

# Set Env

PATH="${TOOLCHAIN}/clang/bin:${TOOLCHAIN}/gcc/bin:${TOOLCHAIN}/gcc_32/bin:${PATH}"
export ARCH=arm64
export KBUILD_BUILD_HOST=danascape
export KBUILD_BUILD_USER="stormCI"
export KBUILD_COMPILER_STRING="${TOOLCHAIN}/clang/bin/clang --version | head -n 1 | sed -e 's/  */ /g' -e 's/[[:space:]]*$//')";

# Build

cd "$KERNEL_DIR"
sendTG "Build Started

Device :- ${DEVICE}
Branch :- ${BRANCH}

<a href=\"${CIRCLE_BUILD_URL}\">circleci</a>"

trackTG "Build Started

Device :- ${DEVICE}
Branch :- ${BRANCH}

${CIRCLE_BUILD_URL}"

if [[ "$DEFCONFIG" == "none" ]];
then
	if [ -f $KERNEL_DIR/arch/arm64/configs/$DEVICE-perf_defconfig ]
	then
	    make O=out ARCH=arm64 $DEVICE-perf_defconfig
	elif [ -f $KERNEL_DIR/arch/arm64/configs/vendor/$DEVICE-perf_defconfig ]
	then
	    make O=out ARCH=arm64 vendor/$DEVICE-perf_defconfig
	else
	    exit 0
	fi
else
	if [ -f $KERNEL_DIR/arch/arm64/configs/$DEFCONFIG-perf_defconfig ]
	then
	     make O=out ARCH=arm64 $DEFCONFIG-perf_defconfig
	elif [ -f $KERNEL_DIR/arch/arm64/configs/vendor/$DEFCONFIG-perf_defconfig ]
	then
	    make O=out ARCH=arm64 vendor/$DEFCONFIG-perf_defconfig
	else
	    exit 0
	fi
fi

START=$(date +"%s")
if [[ "$DEVICE" == "phoenix" || "$DEVICE" == "avicii" ]];
then
     PATH="${TOOLCHAIN}/clang/bin:${PATH}"
     make -j$(nproc --all) O=out ARCH=arm64 CROSS_COMPILE=aarch64-linux-gnu- CROSS_COMPILE_ARM32=arm-linux-gnueabi- CC=clang AR=llvm-ar OBJDUMP=llvm-objdump STRIP=llvm-strip NM=llvm-nm OBJCOPY=llvm-objcopy LD=ld.lld | tee logs.txt
else
     make -j$(nproc --all) O=out ARCH=arm64 CC=clang CLANG_TRIPLE=aarch64-linux-gnu- CROSS_COMPILE=aarch64-linux-android- CROSS_COMPILE_ARM32=arm-linux-androideabi- | tee logs.txt
fi

END=$(date +"%s")
export DIFF=$(($END - $START))
cd $PROJECT_DIR
